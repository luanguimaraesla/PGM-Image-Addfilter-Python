__author__ = 'luan'

from dimensoes import Dimensoes
from copy import copy
from glob import glob

class Imagem:
    """Classe que abrange qualquer tipo de imagem, PGM, PPM, etc..."""

    #Variáveis estáticas da classe Imagem
    _nonSavedFiles = []
    _caminhoDaPastaImagens = "../images/"
    _defaultNameFiles = "new"


    def __init__(self, caminho = None, identificador = None, pixels = None, largura = None, altura = None):
        """Inicialização dos objetos do tipo Imagem"""
        self.setCaminho(caminho) #O caminho da imagem em relação ao arquivo executado
        self._pixels = pixels #Um vetor unidimensional que simula uma matriz de pixels
        self.dimensoes = Dimensoes() #Armazena as dimensões da imagem
        self._identificador = identificador #identificador armazena o tipo da imagem, no caso de uma PGM, seria o valor 'P5'

        if largura and altura:
            self.dimensoes.setBoth(largura, altura)


    def setPixels(self, listaDePixels):
        """Verifica se a listaDePixels é um objeto do tipo list e, caso seja,
        faz uma cópia para self._pixels, isso porque listas em python são mutáveis"""
        if isinstance(listaDePixels, list):
            self._pixels = copy(listaDePixels)
        else:
            exit(1)


    def getCopyOfPixels(self):
        """Retorna uma cópia dos pixels da imagem"""
        return copy(self._pixels)


    def getPixels(self):
        """Retorna os próprios pixels da imagem, dessa forma, se o retorno for modificado,
        as alterações serão percebidas por self._pixels"""
        return self._pixels


    def setIdentificador(self, identificador):
        """Verifica se o identificador é do tipo string, caso contrário encerra a execução do programa"""
        if isinstance(identificador, str):
            self._identificador = identificador
        else:
            exit(1)


    def getIdentificador(self):
        """Retorna o identificador da imagem"""
        return self._identificador


    def _setType(self):
        """Método 'privado' que, a partir do caminho fornecido, salva a extensão da imagem para futuras modificações"""

        #   A ideia por trás desse cálculo é inverter a string caminho, pegar os dígitos até
        #   o primeiro ponto e depois inverter novamente...
        #   Exemplo: ../imagem/abc.img --> gmi.cba/megami/.. --> [gmi.] --> [.img]
        self._type = self._caminho[self._caminho.index('.'):len(self._caminho):-1][::-1]


    def getCaminho(self):
        """Retorna o caminho da imagem"""
        return self._caminho


    def setCaminho(self, caminho):
        """Faz uma série de verificações para validar a entrada de um novo caminho.
        Esse método aceita a entrada do caminho de quatro formas diferentes.

        1. "exemplo.pgm", dessa maneira a única correção que deve ser feita é adicionar
                          o caminho relativo a pasta de imagens, se for a pasta padrão,
                          ficará: "../imagens/exemplo.pgm".

        2. "exemplo2" , dessa maneira, precisamos adicionar, além da pasta padrão, a
                       extensão correta. Resultando em "../imagens/exemplo2.img"

        3. None ou "" , desse modo, precisaremos criar um nome default para o caminho do arquivo,
                        será atribuido então, por padrão, "../imagens/new(x).img" com 0 < x < 100

        4. "../imagens/exemplo4.pgm", dessa maneira não é preciso realizar nenhuma operação sobre
                                        o valor de entrada"""

        files = [] #cria uma lista vazia que armazenará os títulos dos arquivos na pasta de salvamento
        ext = ".img" #cria uma extensão momentânea para casos onde o usuário não forneça um tipo de arquivo

        try: #se a variável self._type já existe, ou seja, se já foi declarada uma extensão, utilize-a
            if self._type:
                ext = self._type
        except AttributeError:
            pass

        #Se o caminho passado for uma string vazia ou None, faça essa rotina
        if not caminho:
            #armazene em files o nome de todas os arquivos existentes no diretório destino, para não sobrescrever nenhum arquivo
            files = glob(self._caminhoDaPastaImagens+ ext)

            for x in range(100): #habilita  a criação de até 100 arquivos
                #caminho passa a se chamar por exemplo ../imagens/new(x).img
                caminho = self._caminhoDaPastaImagens + self._defaultNameFiles + str(x) + ext

                #se o caminho não existir na lista de arquivos criados e também na variável estática que armazena o nome
                #das outras imagens, ele pode ser criado.
                if caminho not in files and caminho not in Imagem._nonSavedFiles:
                    Imagem._nonSavedFiles.append(caminho)
                    break
            else:
                #Se o limite de 100 arquivos for estrapolado, o programa encerrará forçadamente
                exit(1)
        else:
            #caso o nome do arquivo fornecido não seja nulo, precisamos fazer algumas verificações
            #a primeira é se o caminho relativo está correto
            if self._caminhoDaPastaImagens not in caminho[:len(self._caminhoDaPastaImagens)]:
                caminho = self._caminhoDaPastaImagens + caminho

            #a segunda verificação é se a extensão foi declarada junto ao caminho
            if caminho[::-1][3]!= '.' and '.' not in caminho[2:]:
                caminho+=ext

        try: #em caso de uma redefinição de caminho, excluir do cachê o antigo nome
            Imagem._nonSavedFiles.remove(self._caminho)
        except (ValueError, AttributeError):
            pass

        #setar o caminho correto
        self._caminho = caminho
        #setar a extensão do arquivo
        self._setType()

