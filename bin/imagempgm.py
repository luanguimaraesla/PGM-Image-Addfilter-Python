__author__ = 'luan'

from imagem import Imagem
from myexceptions import PGMIOError

class ImagemPGM(Imagem):
    def __init__(self, caminho = None):
        Imagem.__init__(self)
        self.nivelMaximoDeCinza = None

        if caminho:
            self.abrir(caminho)


    def abrir(self, caminho):

        Imagem.setCaminho(self, caminho)

        try:
            with open(self.getCaminho(), 'r') as arquivo:
                linhas = arquivo.readlines()
                for x in linhas:
                    print(x)

        except IOError as arqErr:
            raise arqErr

        identificador = linhas.pop(0).strip()
        print("\n\n" + str(identificador) + "\n\n")
        if identificador != "P5":
            raise PGMIOError

        comentario = ''
        while(linhas[0][0] == '#'):
            comentario += linhas.pop(0)

        (largura, altura) = linhas.pop(0).strip().split(' ')
        largura = int(largura)
        altura = int(altura)
        self.nivelMaximoDeCinza = linhas.pop(0).strip()

        pixels = []
        for x in range(0, len(linhas)):
            y = linhas[x].strip('\n').split(' ')
            pixels.extend([int(k) for k in y])

        self.dimensoes.setBoth(largura, altura)
        self.setIdentificador(identificador)
        self.setPixels(pixels)





